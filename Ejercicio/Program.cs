﻿using System;

namespace Ejercicio
{
    class Program
    {
        static void Main(string[] args)
        {
            Carro MiFerrari = new Carro();
            MiFerrari.mostrarDatos();
            MiFerrari.arrancar();
            MiFerrari.combustible();
        }
    }

    class Carro{
        public string Marca = "Ford";
        public string Modelo = "Ikon";
        public string Año = "2002";
        public string Tipo = "Sedan";
        public int cilindros = 4;
        public string color = "Rojo";
public void mostrarDatos(){
    Console.ForegroundColor = ConsoleColor.Yellow;
    Console.BackgroundColor = ConsoleColor.Black;
    Console.Clear();
    Console.WriteLine("::::::::EJERCICIO 1 CURSO POO::::::::");
    Console.WriteLine("\n");
    Console.ForegroundColor = ConsoleColor.Cyan;
    Console.WriteLine("La Marca es: "+Marca);
    Console.WriteLine("La Modelo es: "+Modelo);
    Console.WriteLine("Año: "+Año);
    Console.WriteLine("Tipo: "+Tipo);
    Console.WriteLine("Cantidad de Cilindros: "+cilindros);
    Console.WriteLine("Color: "+color);
}
public void arrancar(){
    Console.ForegroundColor = ConsoleColor.Red;
    Console.WriteLine("\n");
    Console.WriteLine("Resultado de las Pruebas: Arrancó...");
}
public void combustible(){
    Console.ForegroundColor = ConsoleColor.Green;
    Console.WriteLine("Pruebas de Ahorro de Combustible: Eficiente...");
    Console.WriteLine("\n");
    Console.ForegroundColor = ConsoleColor.White;
    //Autor: Jorge Luis Pérez
        }
    }
}
